package wowdir

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"sort"
)

var (
	directory = flag.String("wowdir", getDefaultDirectory(), "what directory to find WoW")
	edition   = flag.String("edition", "tbc", "What wow edition (tbc,classic,retail)")
	account   = flag.String("account", "", "Which account to use if there are multiple. Default: first alphanumerically. Example: '88412802#2'")
)

var paths = map[string]string{
	"tbc":     "_classic_",
	"classic": "_classic_era_",
	"retail":  "_retail_",
}

func getDefaultDirectory() string {
	switch runtime.GOOS {
	case "windows":
		return "C:/Program Files (x86)/World of Warcraft/"
	case "linux":
		// Because I'm probably the only one doing this
		return "/home/notnarb/Games/world-of-warcraft-classic/drive_c/Program Files (x86)/World of Warcraft/"
	default:
		log.Fatalf("Unknown operating system %s. Provide the wowdir flag", runtime.GOOS)
		panic("")
	}
}

// Returns the base directory containing the WTF folder.
func getBaseDir() (string, error) {
	e, found := paths[*edition]
	if !found {
		return "", fmt.Errorf("Invalid edition %s", *edition)
	}

	base := filepath.Join(*directory, e)

	if _, err := os.Stat(base); os.IsNotExist(err) {
		return "", fmt.Errorf("Could not find base directory: %s", base)
	}

	return base, nil
}

// Returns the path of one of the folders in the specified account base folder.
// TODO: Might make sense to filter for just folders matching the pattern
//       {digits}#{digit}
func getDefaultAccountFolder(accountBaseFolder string) (string, error) {
	options := make([]string, 0)

	files, err := ioutil.ReadDir(accountBaseFolder)
	if err != nil {
		return "", fmt.Errorf("Failed to read account directory %s: %w", accountBaseFolder, err)
	}

	for _, f := range files {
		if f.IsDir() {
			options = append(options, filepath.Join(accountBaseFolder, f.Name()))
		}
	}
	if err != nil {
		return "", fmt.Errorf("Failed to read account directory: %w", err)
	}
	if len(options) == 0 {
		return "", fmt.Errorf("Found no account folders in directory %s", accountBaseFolder)
	}

	sort.Strings(options)
	return options[0], nil
}

// Returns the specific account folder (under WTF/Account) which contains the
// SavedVariables folder.
func getAccountFolder() (string, error) {
	base, err := getBaseDir()
	if err != nil {
		return "", err
	}
	var af string
	accountBaseFolder := filepath.Join(base, "WTF/Account")

	if *account != "" {
		af = filepath.Join(accountBaseFolder, *account)
	} else {
		af, err = getDefaultAccountFolder(accountBaseFolder)
		if err != nil {
			return "", fmt.Errorf("Failed to lookup default folder: %w", err)
		}
	}

	if _, err := os.Stat(af); os.IsNotExist(err) {
		return "", fmt.Errorf("Could not find account directory: %s", af)
	}
	return af, nil
}

// GetVariablePath returns the path of the lua file in the SharedVariables
// folder corresponding to the given addon name.
func GetVariablePath(addonName string) (string, error) {
	af, err := getAccountFolder()
	if err != nil {
		return "", err
	}

	vp := filepath.Join(af, "SavedVariables", fmt.Sprintf("%s.lua", addonName))
	if _, err := os.Stat(vp); os.IsNotExist(err) {
		return "", fmt.Errorf("Could not find variable file: %s", vp)
	}

	return vp, nil
}
