# API configurations cannot have the same id and therefore an error is raised
# when trying to replace one. Add random string to the end in order to make
# in-place upgrades easier.
resource "random_string" "api_config" {
  length = 8
  special = false
  upper = false
  keepers = {
	api_config = local.api_config
  }
}

resource "google_service_account" "api_cfg" {
  account_id   = "luasyncer-api-cfg"
}

resource "google_api_gateway_api" "api_cfg" {
  provider = google-beta
  api_id = "api-cfg"
}

resource "google_api_gateway_api_config" "api_cfg" {
  provider = google-beta
  api = google_api_gateway_api.api_cfg.api_id
  api_config_id = "cfg-${random_string.api_config.result}"

  gateway_config {
	backend_config {
	  google_service_account = google_service_account.api_cfg.email
	}
  }
  openapi_documents {
    document {
      path = "spec.yaml"
	  contents = local.api_config
    }
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "google_api_gateway_gateway" "api_gw" {
  provider = google-beta
  api_config = google_api_gateway_api_config.api_cfg.id
  gateway_id = "api-gw"
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${google_service_account.api_cfg.email}"
}

locals {
  api_config = base64encode(yamlencode({
	swagger = "2.0"
	info = {
	  title = "Lua Syncer API"
	  description = "# Lua Syncer API"
	  version= "0.0.1"
	}
	basePath= "/v1"
	produces = ["application/json"]
	consumes = ["application/json"]
	schemes = ["https"]
	paths = {
	  "/spreadsheets/{spreadsheetId}:sync" = {
		"post" = {
		  summary =  "Updates the spreadsheet."
		  operationId = "SyncSpreadsheet"
		  x-google-backend = {
			address = google_cloudfunctions_function.function.https_trigger_url
		  }
		  responses = {
			200 = {
			  description = "OK"
			}
		  }
		  parameters = [
			{
			  in = "path"
			  name = "spreadsheetId"
			  required = true
			  type = "string"
			  description = "ID of the spreadsheet to update"
			},
			{
			  in = "body"
			  name = "body"
			  required = true
			  schema = {
				"$ref" = "#/definitions/SpreadsheetSync"
			  }
			}
		  ]
		}
	  }
	}
	definitions = {
	  SpreadsheetSync = {
		type = "object"
		properties = {
		  token = {
			type = "string"
		  }
		  sheet = {
			type = "string"
		  }
		  range = {
			type = "string"
		  }
		}
	  }
	}
  }))
}
