terraform {
  backend "gcs" {
    bucket  = "luasyncer-tf"
    prefix  = "terraform/state"
  }
}

provider "google" {
  project = "notnarb-36ea5"
  region  = "us-central1"
  zone    = "us-central1-b"
}

provider "google-beta" {
  project = "notnarb-36ea5"
  region  = "us-central1"
  zone    = "us-central1-b"
}

# Randomize object name so GCF recognizes code updates.
// https://github.com/hashicorp/terraform-provider-google/issues/1938#issuecomment-660609985
resource "random_string" "name" {
  length = 8
  special = false
  upper = false
  keepers = {
    md5 = filemd5("${path.module}/../bin/out.zip")
  }
}

resource "google_storage_bucket" "bucket" {
  name = "luasyncer"
}

resource "google_storage_bucket_object" "archive" {
  name   = "deploy/index-${random_string.name.result}.zip"
  bucket = google_storage_bucket.bucket.name
  source = "${path.module}/../bin/out.zip"
}

resource "google_service_account" "hello_world_gcf" {
  account_id   = "luasyncer-gcf"
}

resource "google_cloudfunctions_function" "function" {
  name        = "sheet-sync-handler"
  description = "Proxies requests to the sheets API to sync CSV files"
  runtime     = "go113"

  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "Handler"

  service_account_email = google_service_account.hello_world_gcf.email
  environment_variables = {
	KEY_COLLECTION = "luasyncer_prod"
  }
}

