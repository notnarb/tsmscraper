resource google_project_iam_member "firestore_user" {
  role   = "roles/datastore.user"
  member = "serviceAccount:${google_service_account.hello_world_gcf.email}"
}
