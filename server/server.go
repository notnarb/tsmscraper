// Package server contains an HTTP Cloud Function.
package server

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/sheets/v4"
)

const KEY_COLLECTION_ENV_NAME = "KEY_COLLECTION"

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type SyncSheetRequest struct {
	Token string `json:"token"`
	Sheet string `json:"sheet"`
	Range string `json:"range"`
}

type SyncSheetResponse struct {
	Response string `json:"response"`
}

type FirestoreKey struct {
	// The ID of the spreadshheet this token is valid for.
	SpreadsheetId string `firestore:"spreadsheetid"`
}

func writeErrorMessage(w http.ResponseWriter, statusCode int, err string) {
	foo := ErrorResponse{
		Code:    statusCode,
		Message: err,
	}
	w.WriteHeader(statusCode)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(foo)
}

func writeSuccessResponse(w http.ResponseWriter, response string) {
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")

	var d SyncSheetResponse
	d.Response = response
	json.NewEncoder(w).Encode(d)
}

func Handler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	var d SyncSheetRequest
	spreadsheetId, found := r.URL.Query()["spreadsheetId"]
	if !found || len(spreadsheetId) != 1 {
		writeErrorMessage(w, http.StatusBadRequest, "Missing required parameter {spreadsheetId}")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		writeErrorMessage(w, http.StatusBadRequest, fmt.Sprintf("Failed to parse JSON: %v", err))
		return
	}

	validReq, err := isValidKey(ctx, d.Token, spreadsheetId[0])
	if err != nil {
		writeErrorMessage(w, http.StatusUnauthorized, fmt.Sprintf("Failed to lookup token: %v", err))
		return
	}

	if !validReq {
		writeErrorMessage(w, http.StatusForbidden, fmt.Sprintf("Token is not applicable for this sheet id"))
		return
	}

	if err := writeSheet(ctx, spreadsheetId[0], d.Range, d.Sheet); err != nil {
		writeErrorMessage(w, http.StatusInternalServerError, fmt.Sprintf("Failed to write sheet: %v", err))
		return
	}
	writeSuccessResponse(w, "success!")
}

func csvToValueRange(c string) *sheets.ValueRange {
	var vr sheets.ValueRange

	r := csv.NewReader(strings.NewReader(c))
	for {
		record, err := r.Read()
		fmt.Println("reading record", record)
		if err == io.EOF {
			break
		}
		row := make([]interface{}, len(record))
		for k, v := range record {
			row[k] = v
		}
		fmt.Println("appending row", row)
		vr.Values = append(vr.Values, row)
	}

	return &vr
}

func writeSheet(ctx context.Context, spreadsheetId string, spreadsheetRange string, body string) error {
	srv, err := sheets.NewService(ctx)
	if err != nil {
		return fmt.Errorf("Failed to connect to connect to sheets API: %w", err)

	}

	_, err = srv.Spreadsheets.Values.Update(spreadsheetId, spreadsheetRange, csvToValueRange(body)).ValueInputOption("USER_ENTERED").Do()
	if err != nil {
		return fmt.Errorf("Unable to retrieve data from sheet: %w", err)
	}

	fmt.Println("Sheet Synced")

	return nil
}

// Returns the full path (collection + key id) for the given key id.
func buildKeyPath(key string) (string, error) {
	prefix, found := os.LookupEnv(KEY_COLLECTION_ENV_NAME)
	if !found {
		return "", fmt.Errorf("Missing required environment variable: %s", KEY_COLLECTION_ENV_NAME)
	}
	return fmt.Sprintf("%s/%s", prefix, key), nil
}

// Returns true if the specified key is valid for the specified spreadsheet.
func isValidKey(ctx context.Context, key string, spreadsheetId string) (bool, error) {
	client, err := firestore.NewClient(ctx, firestore.DetectProjectID)
	if err != nil {
		return false, fmt.Errorf("Failed to create firestore client: %w", err)
	}

	kp, err := buildKeyPath(key)
	if err != nil {
		return false, err
	}

	doc, err := client.Doc(kp).Get(ctx)
	if err != nil {
		return false, fmt.Errorf("Failed to find key %s: %w", kp, err)
	}

	var fsKey FirestoreKey
	if err := doc.DataTo(&fsKey); err != nil {
		return false, fmt.Errorf("Failed to parse key %s: %w", kp, err)
	}

	return fsKey.SpreadsheetId == spreadsheetId, nil
}
