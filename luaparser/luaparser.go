package luaparser

import (
	_ "embed"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"

	lua "github.com/yuin/gopher-lua"
)

var (
	isArrayItem = regexp.MustCompile(`-- \[.*\]\s+$`)
	isProperty  = regexp.MustCompile(`(\[.*\] = [^,]+)`)
	numKeyRegex = regexp.MustCompile(`\[([0-9]+)\]`)
)

//go:embed json.lua
var jsonLua string

// Loads the embeded json.lua library under the variable 'json'.
func loadJsonLibrary(L *lua.LState) error {
	e, err := L.LoadString(jsonLua)
	if err != nil {
		return fmt.Errorf("Failed to read embeded json library: %w", err)
	}
	L.SetGlobal("loadJson", e)

	if err := L.DoString("json = loadJson()"); err != nil {
		return fmt.Errorf("Failed to import json library, %w", err)
	}

	return nil
}

// Returns a new lua string with the types that are unable to be converted to
// JSON to types that are.
func sanitizeLua(i string) string {
	// Convert all numerical keys to values
	r := numKeyRegex.ReplaceAllString(i, "[\"$1\"]")

	// Wrap properties embedded in an array into an object.
	// e.g. {"a" -- [1]\n, "b" -- [2]\n, ["property"] = true} =>
	//      {"a" -- [1]\n, "b" -- [2]\n, {["property] = true}}
	lines := strings.Split(r, "\n")
	prevLine := ""
	for index, line := range lines {
		if isArrayItem.MatchString(prevLine) {
			if m := isProperty.FindStringSubmatch(line); len(m) > 0 {
				lines[index] = fmt.Sprintf("{%s}", m[0])
			}
		}
		prevLine = line
	}
	return strings.Join(lines, "\n")
}

// LuaToJson reads specified variable (database) from specified lua file (file)
// and returns that variable converted to JSON.
func LuaToJson(file string, database string) (string, error) {
	L := lua.NewState()
	defer L.Close()

	loadJsonLibrary(L)

	retTable := L.NewTable()
	L.SetGlobal("ret", retTable)

	f, err := ioutil.ReadFile(file)
	if err != nil {
		return "", fmt.Errorf("failed to read file %s: %w", file, err)
	}

	if err := L.DoString(sanitizeLua(string(f))); err != nil {
		return "", fmt.Errorf("Failed to execute provided lua file %s: %w", file, err)
	}

	writeJson := fmt.Sprintf("ret['json'] = json.encode(%s)", database)
	if err := L.DoString(writeJson); err != nil {
		return "", fmt.Errorf("json encode error: %w", err)
	}

	ret := retTable.RawGet(lua.LString("json")).String()
	if ret == "null" {
		return "", fmt.Errorf("Failed to find table %s", database)
	}
	return ret, nil
}
