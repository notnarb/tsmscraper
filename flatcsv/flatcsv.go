package flatcsv

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
)

// Fetches the top-level properties in each row in list of JSON objects and
// returns them as a sorted list.
// Expects each row to be a JSON object with string keys.
func GetSortedHeaders(rows []interface{}) []string {
	fieldSet := make(map[string]bool)

	for _, row := range rows {
		x, found := row.(map[string]interface{})
		if !found {
			log.Fatalf("Invalid JSON entry %v", x)
		}
		for k, _ := range x {
			fieldSet[k] = true
		}
	}
	fields := make([]string, 0)
	for k, _ := range fieldSet {
		fields = append(fields, k)
	}

	sort.Strings(fields)
	return fields
}

// Writes the specified rows to a filename. First row contains column
// headers. Assumes that each row/object is flat and doesn't have nested fields.
func WriteCSVFile(filename string, rows []interface{}) {
	file, err := os.Create(filename)
	if err != nil {
		log.Fatalf("Failed to open file %s %v", filename, err)
	}
	defer file.Close()

	WriteCSV(file, rows)
}

func WriteCSV(w io.Writer, rows []interface{}) {
	writer := csv.NewWriter(w)

	names := GetSortedHeaders(rows)
	writer.Write(names)

	for _, row := range rows {
		r := make([]string, 0)
		x := row.(map[string]interface{})
		for _, name := range names {
			if value, found := x[name]; found {
				r = append(r, fmt.Sprint(value))
			} else {
				r = append(r, "")
			}
		}
		writer.Write(r)
	}
	writer.Flush()
}
