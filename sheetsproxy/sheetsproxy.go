package sheetsproxy

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"

	"notnarb.com/tsmscraper/flatcsv"
	"notnarb.com/tsmscraper/server"
)

const defaultProxy = "https://api-gw-c6ygfchy.uc.gateway.dev/v1/spreadsheets/{spreadsheetId}:sync"

var (
	endpoint = flag.String("sheetproxy", defaultProxy, "HTTP endpoint to post sheet requests to. {spreadsheetId} will be substituted with the spreadsheet ID")
)

func generateUrl(sheetId string) string {
	return strings.Replace(*endpoint, "{spreadsheetId}", sheetId, 1)
}

func validateParams(sheetId string, sheetRange string, token string, rows []interface{}) {
	if len(sheetId) == 0 {
		log.Fatalf("Missing 'sheetid' field")
	} else if len(sheetRange) == 0 {
		log.Fatalf("Missing 'sheetrange' field")
	} else if len(token) == 0 {
		log.Fatalf("Missing 'token' field")
	} else if len(rows) == 0 {
		log.Fatalf("No rows to write!")
	}
}

// Writes to Google Sheets via an HTTP endpoint specified by the sheetproxy
// flag. The payload is uploaded in CSV format.
func WriteSheetProxy(sheetId string, sheetRange string, token string, rows []interface{}) {
	validateParams(sheetId, sheetRange, token, rows)

	sheet := strings.Builder{}
	flatcsv.WriteCSV(&sheet, rows)

	url := generateUrl(sheetId)

	req := server.SyncSheetRequest{
		Token: token,
		Sheet: sheet.String(),
		Range: sheetRange,
	}

	body, err := json.Marshal(req)
	if err != nil {
		log.Fatalf("Failed to Encode request: %v", err)
	}

	log.Println("Sending request to", url)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Fatalf("Failed to perform HTTP request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		dump, _ := httputil.DumpResponse(resp, true)
		log.Fatalf("Invalid HTTP status received (%v):\n%v", resp.StatusCode, string(dump))
	}

	log.Println("Sheet uploaded")
}
