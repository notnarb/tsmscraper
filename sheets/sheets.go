package sheets

import (
	"log"

	"notnarb.com/tsmscraper/flatcsv"

	"golang.org/x/net/context"
	"google.golang.org/api/sheets/v4"
)

// Converts a flat json object to a sheets ValueRange
func jsonToValueRange(rows []interface{}) *sheets.ValueRange {
	var vr sheets.ValueRange

	header := flatcsv.GetSortedHeaders(rows)

	hr := make([]interface{}, len(header))
	for k, v := range header {
		hr[k] = v
	}
	vr.Values = append(vr.Values, hr)

	for _, row := range rows {
		raw := make([]interface{}, len(header))
		r := row.(map[string]interface{})

		for i, v := range header {
			if value, found := r[v]; found {
				raw[i] = value
			}
		}
		vr.Values = append(vr.Values, raw)
	}

	return &vr
}

// Expects to use service account specified in GOOGLE_APPLICATION_CREDENTIALS.
func WriteSheet(spreadsheet string, destRange string, rows []interface{}) {
	context := context.Background()

	srv, err := sheets.NewService(context)
	if err != nil {
		log.Fatalf("Failed to connect to connect to sheets API: %v", err)
	}

	_, err = srv.Spreadsheets.Values.Update(spreadsheet, destRange, jsonToValueRange(rows)).ValueInputOption("RAW").Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet: %v", err)
	}

	log.Println("Sheet Synced")
}
