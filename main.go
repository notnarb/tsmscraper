package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	"notnarb.com/tsmscraper/config"
	"notnarb.com/tsmscraper/flatcsv"
	"notnarb.com/tsmscraper/luaparser"
	"notnarb.com/tsmscraper/sheets"
	"notnarb.com/tsmscraper/sheetsproxy"
	"notnarb.com/tsmscraper/wowdir"

	"github.com/itchyny/gojq"
)

var (
	configFile = flag.String("config", "config.yml", "what config file to read")
)

// usage prints the usage commands and quits.
func usage(message string) {
	flag.Usage()
	fmt.Println("\nUsage ./main [flags] <sync|dump>")
	if message != "" {
		fmt.Println(message)
	}
	os.Exit(1)
}

func assertArgLength(expect int, example string) {
	if check := len(flag.Args()); check != expect {
		message := fmt.Sprintf("Expected to see %v args but got %v\nExample: %s", expect, check, example)
		usage(message)
	}
}

func main() {
	flag.Parse()

	switch x := flag.Arg(0); x {
	case "sync":
		assertArgLength(1, "./main -account=123567#0 sync")
		start()
	case "dump":
		assertArgLength(3, "./main dump DBM-Core DBM_MinimapIcon")
		printJson(flag.Arg(1), flag.Arg(2))
	default:
		usage("Missing command")
	}
}

// runJq evaluates a JQ expression against a json input and returns the expressions return value.
func runJq(expression string, input interface{}) []interface{} {
	query, err := gojq.Parse(expression)
	if err != nil {
		log.Fatalln(err)
	}

	out := make([]interface{}, 0)

	iter := query.Run(input)
	for {
		v, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := v.(error); ok {
			log.Fatalln(err)
		}
		out = append(out, v)
	}

	return out
}

// Reads the saved variable for a given addon and prints the internal json
// representation passed to jq.
func printJson(addonName string, variableName string) {
	f, err := wowdir.GetVariablePath(addonName)
	if err != nil {
		log.Fatalf("Failed to find variable file: %v", err)
	}
	str, err := luaparser.LuaToJson(f, variableName)
	if err != nil {
		log.Fatalf("Failed to convert lua file to json: %v", err)
	}
	fmt.Println(str)
}

func start() {
	config, err := config.ParseConfigFile(*configFile)
	if err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}

	for _, c := range *config {
		f, err := wowdir.GetVariablePath(c.Addon)
		if err != nil {
			log.Fatalf("Failed to find variable file: %v", err)
		}
		str, err := luaparser.LuaToJson(f, c.Variable)
		if err != nil {
			log.Fatalf("Failed to convert lua file to json: %v", err)
		}

		var out interface{}
		json.Unmarshal([]byte(str), &out)

		ret := runJq(c.Expression, out)

		switch c.Output.Type {
		case "flatcsv":
			flatcsv.WriteCSVFile(c.Output.Name, ret)
		case "sheets":
			sheets.WriteSheet(c.Output.Sheetid, c.Output.Sheetrange, ret)
		case "sheetsproxy":
			sheetsproxy.WriteSheetProxy(c.Output.Sheetid, c.Output.Sheetrange, c.Output.Token, ret)
		default:
			log.Fatalf("Unknown export type: %s", c.Output.Type)
		}
	}
}
