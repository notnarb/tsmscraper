module notnarb.com/tsmscraper

go 1.16

require (
	cloud.google.com/go/firestore v1.5.0
	github.com/itchyny/gojq v0.12.3
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/sys v0.0.0-20210305230114-8fe3ee5dd75b // indirect
	google.golang.org/api v0.40.0
	gopkg.in/yaml.v2 v2.4.0
)
