package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type ExporterConfig struct {
	Addon      string
	Expression string
	Variable   string
	Output     ExporterOutput
}
type ExporterOutput struct {
	Type       string
	Name       string
	Sheetrange string
	Sheetid    string
	Token      string // Auth for sheetproxy
}

type ConfigFileFormat struct {
	Exporters []ExporterConfig
}

// Parses the specified yml file and returns a list of configured export targets.
func ParseConfigFile(file string) (*[]ExporterConfig, error) {
	f, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("Failed to read yaml file %s: %w", file, err)
	}

	out := ConfigFileFormat{}

	err = yaml.Unmarshal([]byte(f), &out)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse yaml file %s: %w", file, err)
	}

	if len(out.Exporters) == 0 {
		return nil, fmt.Errorf("Failed to find exporters in file %s", file)
	}

	return &out.Exporters, nil
}
